# PortAudio/minimp3 player example

## Dependencies

  + PortAudio headers and libs (and a backend)

## Quick start

```bash
$ git clone --recursive git@github.com:herrhotzenplotz/mmp3player
$ cd mmp3player/
$ make
$ ./mmp3player <mp3-file>
```
