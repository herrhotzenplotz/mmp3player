/* Copyright 2020 Nico Sonack
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

/* minimp3 seeking example
 *
 * This example seeks into an mp3 file and streams it using PortAudio
 * for playback.
 */

#include <stdio.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <unistd.h>

#define MINIMP3_IMPLEMENTATION
#include "minimp3/minimp3_ex.h"

#include <portaudio.h>

int main(argc, argv)
     int argc;
     char** argv;
{
    if (argc < 4)
        {
            fprintf(stderr, "ERR : seek requires 4 arguments: <file> <start [sec]> <end [sec]>\n");
            exit(EXIT_FAILURE);
        }

    static mp3dec_ex_t dec;

    if (mp3dec_ex_open(&dec, argv[1], MP3D_SEEK_TO_SAMPLE)) {
        fprintf(stderr, "ERR : Cannot open file.\n");
        exit(EXIT_FAILURE);
    }

    int start = atoi(argv[2]),
        end = atoi(argv[3]);

    PaError error;
    PaStream *stream = NULL;

    error = Pa_Initialize();
    if (error != paNoError) {
        fprintf(stderr, "ERR : Couldn't initialize PortAudio: %s\n", Pa_GetErrorText(error));
        goto err_ini;
    }

    error = Pa_OpenDefaultStream(&stream, 0,
                                 dec.info.channels,
                                 paInt16,
                                 dec.info.hz,
                                 MINIMP3_MAX_SAMPLES_PER_FRAME, NULL, NULL);
    if (error != paNoError) {
        fprintf(stderr, "ERR : Couldn't connect to PortAudio: %s\n", Pa_GetErrorText(error));
        goto err_pa;
    }

    fprintf(stdout, "INF : Encoder gave sample rate of %d Hz.\n", dec.info.hz);

    size_t expected_frames = dec.info.hz * (end - start);
    size_t offset_samples = dec.info.hz * dec.info.channels * start;
    size_t expected_samples = expected_frames * dec.info.channels;

    mp3d_sample_t *buffer = calloc(expected_samples, sizeof(mp3d_sample_t));

    if (mp3dec_ex_seek(&dec, offset_samples)) {
        fprintf(stderr, "ERR : Unable to seek in mp3 stream\n");
        goto err;
    }

    size_t read_samples = mp3dec_ex_read(&dec, buffer, expected_samples);

    if (read_samples != expected_samples) {
        if (dec.last_error) {
            fprintf(stderr, "ERR : Couldn't decode mp3 stream.");
            goto err;
        }
    }

    error = Pa_StartStream(stream);
    if (error != paNoError) {
        fprintf(stderr, "ERR : Couldn't start PortAudio stream: %s\n", Pa_GetErrorText(error));
        goto err;
    }

    error = Pa_WriteStream(stream, buffer, expected_frames);
    if (error != paNoError) {
        fprintf(stderr, "ERR : Unable to send data to PortAudio: %s\n", Pa_GetErrorText(error));
        goto err;
    }

    error = Pa_StopStream(stream);
    if (error != paNoError) {
        fprintf(stderr, "ERR : Unable to drain data to PortAudio: %s\n", Pa_GetErrorText(error));
        goto err;
    }

 err:
    free(buffer);
 err_pa:
    Pa_CloseStream(stream);
    mp3dec_ex_close(&dec);
    Pa_Terminate();
 err_ini:
    ;
}
