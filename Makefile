PKGS = portaudio-2.0

CC ?= gcc
CFLAGS = -Wall -Wextra -Werror -fno-builtin -O0 `pkg-config --cflags $(PKGS)` -ggdb

LIBS=`pkg-config --libs $(PKGS)`

all: mmp3player Makefile

.PHONY: all

mmp3player: main.o
	$(CC) main.o $(LIBS) -o $@

seek: seek.o
	$(CC) seek.o $(LIBS) -o $@

seek.o: seek.c
	$(CC) -c $(CFLAGS) $< -o $@

main.o: main.c
	$(CC) -c $(CFLAGS) $< -o $@
